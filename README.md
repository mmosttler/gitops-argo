# gitops-argo

https://argoproj.github.io/argo-cd/getting_started/
https://github.com/argoproj/argocd-example-apps

```
argocd login
admin
admin
```

```
export ARGOCD_OPTS='--port-forward-namespace argocd'
```

1. After ArgoCD install and cli setup you must then register each application to be deployed by Argo with either the UI or CLI.
'''
argocd app create guestbook.guestbook-plain --repo https://gitlab.com/mmosttler/gitops-argo.git --path workloads/guestbook-plain/guestbook --dest-server https://kubernetes.default.svc --dest-namespace guestbook-plain
'''
2.  `argocd app sync guestbook` to sync the application.

OR

Define declaratively
1. Create an App of Apps for the cluster.  Then register and sync that app.
```
argocd app create apps \
    --dest-namespace argocd \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/mmosttler/gitops-argo.git \
    --path apps  
argocd app sync apps 
```  
